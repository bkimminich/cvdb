'use strict';

describe('Service: ResumeService', function () {

  // load the service's module
  beforeEach(module('cvdbClientApp'));

  // instantiate service
  var ResumeService;
  beforeEach(inject(function (_ResumeService_) {
    ResumeService = _ResumeService_;
  }));

  it('should do something', function () {
    expect(!!ResumeService).toBe(true);
  });

});
