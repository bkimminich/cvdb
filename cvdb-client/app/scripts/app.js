'use strict';

angular.module('cvdbClientApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'ui.bootstrap',
    'ngAnimate',
    'toaster'
])
    .config(['$httpProvider', '$routeProvider', function ($httpProvider, $routeProvider) {
        // Spring security will respond with a "WWW-Authenticate" header
        // if the "X-Request-With" header is not sent in the request.
        // This will in turn trigger a login popup in the browser.
        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        $routeProvider
            .otherwise({
                // default view to be used if no other URL matcher fits
                redirectTo: '/resume'
            });
    }])
    .run(function () {
        /* global FastClick: false */
        FastClick.attach(document.body);
    });
