'use strict';

angular.module('cvdbClientApp').controller('HeaderController', ['$scope', '$location', function ($scope, $location) {

	$scope.hideNavbar = function () {
		var locationPath = $location.path();
		return locationPath.lastIndexOf('/auth', 0) === 0;
	};

	$scope.navbarActive = function (path) {
		var locationPath = $location.path();
		if (locationPath === path || locationPath.substr(0, path.length + 1) === path + '/') {
			return 'active';
		} else {
			return '';
		}
	};

}]);

