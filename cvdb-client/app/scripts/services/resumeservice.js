'use strict';

angular.module('cvdbClientApp')
		.service('ResumeService', ['$resource', function ($resource) {

			this._apiUrl = '/api/resume';
			this._apiUrlId = this._apiUrl + '/:id';

			this._storedPage = 0;
			this._searchString = '';
			this._sort = 'firstName';
			this._order = 'asc';

			this.getStoredPage = function () {
				return this._storedPage;
			};

			this.setStoredPage = function (page) {
				this._storedPage = page;
			};

			this.getSearchString = function () {
				return this._searchString;
			};

			this.setSearchString = function (searchString) {
				this._searchString = searchString;
			};

			this.getSort = function () {
				return this._sort;
			};

			this.setSort = function (sort) {
				this._sort = sort;
			};

			this.getOrder = function () {
				return this._order;
			};

			this.setOrder = function (order) {
				this._order = order;
			};

			this.list = function (page, itemsPerPage) {
				var service = this;
				var resource = $resource(this._apiUrl);
				var searchString = service.getSearchString();
				var queryMap = {page: page, size: itemsPerPage};
				if (searchString !== undefined && searchString.length > 0) {
					queryMap.search = searchString;
				}
				queryMap.sort = service.getSort();
				queryMap.order = service.getOrder();
				return resource.get(queryMap);
			};

			this.get = function (id) {
				var resource = $resource(this._apiUrlId);
				return resource.get({id: id});
			};

			this.remove = function (id) {
				var resource = $resource(this._apiUrlId);
				return resource.delete({id: id});
			};

			this.save = function (resume) {
				var resource = $resource(this._apiUrl);
				return resource.save(resume);
			};

		}]);
