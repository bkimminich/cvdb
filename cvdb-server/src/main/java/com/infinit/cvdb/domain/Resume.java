package com.infinit.cvdb.domain;

import javax.persistence.*;

/**
 * Resume that has a CV
 *
 * Created by owahlen on 31.12.13.
 */
@Entity
public class Resume {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private Long id;

	@Column(nullable = false)
	private String firstName;

	@Column(nullable = false)
	private String lastName;

	public Long getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return getFirstName()+" "+getLastName();
	}

}
