package com.infinit.cvdb.config;

import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.AbstractHttp11Protocol;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.MimeMappings;
import org.springframework.boot.context.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.io.File;

@Configuration
@EnableJpaRepositories("com.infinit.cvdb.repository")
@EnableAutoConfiguration
@ComponentScan("com.infinit.cvdb")
@EntityScan(basePackages = {"com.infinit.cvdb.domain"})
public class Application implements EmbeddedServletContainerCustomizer {

    // As a default it is assumed that the document root is the app folder of the client project.
    // Relative to the root of the project this is located in the following path:
    private final static String CLIENT_DOCUMENT_ROOT = "cvdb-client/app";

    /**
     * When running with an embedded servlet container additional configurations can be applied.
     *
     * @param container that is subject of the configuration
     */
    @Override
    public void customize(ConfigurableEmbeddedServletContainer container) {

        // The embedded servlet container shall use the resources from the client project
        configureDocumentRoot(container);

        if (container instanceof TomcatEmbeddedServletContainerFactory) {
            TomcatEmbeddedServletContainerFactory tomcatContainer = (TomcatEmbeddedServletContainerFactory) container;
            TomcatConnectorCustomizer connectorCustomizer = new TomcatConnectorCustomizer() {
                @Override
                public void customize(Connector connector) {
                    // gzip files larger than specified bytes
                    AbstractHttp11Protocol httpProtocol = (AbstractHttp11Protocol) connector.getProtocolHandler();
                    httpProtocol.setCompression("on");
                    httpProtocol.setCompressionMinSize(256);
                    String mimeTypes = httpProtocol.getCompressableMimeTypes();
                    String mimeTypesWithJson = mimeTypes + ",text/css,text/javascript,application/javascript,application/json";
                    httpProtocol.setCompressableMimeTypes(mimeTypesWithJson);
                }
            };
            tomcatContainer.addConnectorCustomizers(connectorCustomizer);
        }

        // send charset in Content-Type response header to improve performance
        MimeMappings mappings = new MimeMappings(MimeMappings.DEFAULT);
        mappings.add("html", "text/html;charset=utf-8");
        container.setMimeMappings(mappings);
    }

    /**
     * An embedded container is started, when the application is run via the main method.
     * It can be started with the gradle command bootRun
     *
     * @param args start parameters
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        SpringApplication app = new SpringApplication(Application.class);
        app.run(args);
    }

    /**
     * Sets the document root to the resource folder of the client project if available.
     * This allows for instant reloading when developing the app.
     */
    private void configureDocumentRoot(ConfigurableEmbeddedServletContainer container) {
        String documentRootPath = CLIENT_DOCUMENT_ROOT;
        File documentRoot = new File(documentRootPath);
        if (documentRoot.isDirectory() && documentRoot.canRead()) {
            container.setDocumentRoot(documentRoot);
        }
    }
}
