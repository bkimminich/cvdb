package com.infinit.cvdb.controller

import com.infinit.cvdb.domain.Resume
import com.infinit.cvdb.repository.ResumeRepository
import com.infinit.cvdb.test.TestUtil
import com.infinit.cvdb.test.WebTestSpec
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.ResultActions

import static org.hamcrest.Matchers.*
import static org.hamcrest.core.IsNot.not
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

/**
 * Test for the ResumeController
 * Created by owahlen on 29.07.14.
 */
class ResumeControllerSpec extends WebTestSpec {

	private ResumeController resumeController

	@Autowired
	ResumeRepository resumeRepository

	def setup() {
		resumeController = new ResumeController()
	}

	def "unauthorized get should return HttpStatus.UNAUTHORIZED"() {
		when:
		ResultActions result = mockMvc.perform(post('/api/resume')
		)

		then:
		result.andExpect(status().isUnauthorized())

	}

	def "authorized user should get 20 results"() {
		when:
		ResultActions result = mockMvc.perform(get('/api/resume')
				.contentType(MediaType.APPLICATION_JSON)
				.session(user())
		)

		then:
		result.andExpect(status().isOk())
		result.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
		result.andExpect(jsonPath('$.content', hasSize(20)))
	}

	def "get on single resume should have attributes set"() {
		when:
		ResultActions result = mockMvc.perform(get('/api/resume/1')
				.contentType(MediaType.APPLICATION_JSON)
				.session(user())
		)

		then:
		result.andExpect(status().isOk())
		result.andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
		result.andExpect(jsonPath('$.id', equalTo(1)))
		result.andExpect(jsonPath('$.firstName', not(isEmptyOrNullString())))
		result.andExpect(jsonPath('$.lastName', not(isEmptyOrNullString())))
	}

	def "post on existing resume should modifies attributes"() {
		setup:
		Resume originalResume = resumeRepository.findOne(1L)
		assert 'there must be a resume with id 1': originalResume

		when:
		ResultActions result = mockMvc.perform(post('/api/resume')
				.contentType(MediaType.APPLICATION_JSON)
				.session(user())
				.content(TestUtil.convertObjectToJsonBytes(
				[
						id       : 1,
						firstName: originalResume.firstName + '_modified',
						lastName : originalResume.lastName + '_modified'
				]))
		)

		then:
		result.andExpect(status().isOk())
		Resume modifiedResume = resumeRepository.findOne(1L)
		assert 'resume still exists': modifiedResume
		assert 'resume firstName is modified': modifiedResume.firstName == originalResume.firstName + '_modified'
		assert 'resume lastName is modified': modifiedResume.lastName == originalResume.lastName + '_modified'
	}

	def "admin can delete a resume"() {
		setup:
		Resume originalResume = resumeRepository.findOne(1L)
		assert 'there must be a resume with id 1': originalResume

		when:
		ResultActions result = mockMvc.perform(delete('/api/resume/1')
				.contentType(MediaType.APPLICATION_JSON)
				.session(admin())
		)

		then:
		result.andExpect(status().isNoContent())
		assert 'resume no longer exists': resumeRepository.findOne(1L) == null
	}

	def "users are not allowed to delete resumes"() {
		setup:
		Resume originalResume = resumeRepository.findOne(1L)
		assert 'there must be a resume with id 1': originalResume

		when:
		ResultActions result = mockMvc.perform(delete('/api/resume/1')
				.contentType(MediaType.APPLICATION_JSON)
				.session(user())
		)

		then:
		result.andExpect(status().isForbidden())
		assert 'resume still exists': resumeRepository.findOne(1L)
	}

}
